<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Location;
use App\Entity\Voiture;

class VoitureTest extends TestCase
{

    public function testSerieIsString()
    {
        $voiture = new Voiture();
        $voiture->setSerie('ABC123');
        $this->assertIsString($voiture->getSerie());
    }

    public function testDateMiseEnMarcheIsDate()
    {
        $voiture = new Voiture();
        $date = new \DateTime();
        $voiture->setDateMiseEnMarche($date);
        $this->assertInstanceOf(\DateTime::class, $voiture->getDateMiseEnMarche());
    }

    public function testModeleIsString()
    {
        $voiture = new Voiture();
        $voiture->setModele('MonModele');
        $this->assertIsString($voiture->getModele());
    }

    public function testPrixJourIsNumeric()
    {
        $voiture = new Voiture();
        $voiture->setPrixJour(100.00);
        $this->assertIsNumeric($voiture->getPrixJour());
    }

    public function testLocationsOneToMany()
    {
        $voiture = new Voiture();

        // Créez une location de test
        $location = new Location();
        $location->setDateDebut(new \DateTime('2024-01-01'));
        $location->setDateRetour(new \DateTime('2024-01-10'));
        $location->setPrix(200.00);

        // Associez la location à la voiture
        $voiture->addLocation($location);

        // Vérifiez que la relation OneToMany fonctionne
        $this->assertCount(1, $voiture->getLocations());
        $this->assertSame($voiture, $location->getVoiture());
    
    }   






}
