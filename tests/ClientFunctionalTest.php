<?php

namespace App\Tests;
use App\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClientFunctionalTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $client->request('GET', '/client/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Client');
    }

    public function testNew()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/client/new');
    
        $form = $crawler->selectButton('Save')->form();
    
        // Populate form fields with necessary data
        $formData = [
            'client[nom]' => 'John', // Replace with actual data
            'client[prenom]' => 'Doe', // Replace with actual data
            'client[adresse]' => '123 Main St', // Replace with actual data
            'client[cin]' => 'ABC123', // Replace with actual data
        ];
    
        $client->submit($form, $formData);
    
        // Check if the form submission was successful
        $this->assertResponseRedirects('/client/'); // Assuming it redirects to the index page
    
        // Follow the redirect to check the final response
        $client->followRedirect();
    
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Client index');

    }

    public function testShow()
    {
    
        $client = static::createClient();

        // Assuming there is at least one client in the database
        $client->request('GET', '/client/1');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Client');
    

    }

    
    public function testDelete()
    {
        $client = static::createClient();
        $client->request('POST', '/client/1');

        $this->assertResponseRedirects('/client/');
    }
}
