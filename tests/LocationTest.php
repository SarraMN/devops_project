<?php

namespace App\Tests;

use App\Entity\Location;
use App\Entity\Voiture;
use App\Entity\Client;
use PHPUnit\Framework\TestCase;

class LocationTest extends TestCase
{
    public function testDateDebutIsDate()
    {
        $location = new Location();
        $dateDebut = new \DateTime('2024-01-01');
        $location->setDateDebut($dateDebut);
        $this->assertInstanceOf(\DateTime::class, $location->getDateDebut());
    }

    public function testDateFinIsDate()
    {
        $location = new Location();
        $dateFin = new \DateTime('2024-01-10');
        $location->setDateRetour($dateFin);
        $this->assertInstanceOf(\DateTime::class, $location->getDateRetour
        ());
    }

    public function testPrixIsNumeric()
    {
        $location = new Location();
        $location->setPrix(200.00);
        $this->assertIsNumeric($location->getPrix());
    }

    public function testVoitureManyToOne()
    {
        $location = new Location();
        $voiture = new Voiture();

        // Associez la location à la voiture
        $location->setVoiture($voiture);

        // Vérifiez que la relation ManyToOne fonctionne
        $this->assertSame($voiture, $location->getVoiture());
    }

    public function testClientManyToOne()
    {
        $location = new Location();
        $client = new Client();

        // Associez la location au client
        $location->setClient($client);

        // Vérifiez que la relation ManyToOne avec Client fonctionne
        $this->assertSame($client, $location->getClient());
    }
}
