<?php

namespace App\Tests;

use App\Entity\Client;
use App\Entity\Location;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testCinIsString()
    {
        $client = new Client();
        $client->setCin('AB123456');
        $this->assertIsString($client->getCin());
    }

    public function testNomIsString()
    {
        $client = new Client();
        $client->setNom('Doe');
        $this->assertIsString($client->getNom());
    }

    public function testPrenomIsString()
    {
        $client = new Client();
        $client->setPrenom('John');
        $this->assertIsString($client->getPrenom());
    }

    public function testAdresseIsString()
    {
        $client = new Client();
        $client->setAdresse('123 Main Street');
        $this->assertIsString($client->getAdresse());
    }

    public function testLocationsOneToMany()
    {
        $client = new Client();

        // Créez une location de test
        $location = new Location();
        $location->setDateDebut(new \DateTime('2024-01-01'));
        $location->setDateRetour(new \DateTime('2024-01-10'));
        $location->setPrix(200.00);

        // Associez la location au client
        $client->addLocation($location);

        // Vérifiez que la relation OneToMany avec Location fonctionne
        $this->assertCount(1, $client->getLocations());
        $this->assertSame($client, $location->getClient());
    }
}